<?php

require './vendor/autoload.php';

use Pizzaria\Pedido;
use Pizzaria\Pizza\Pizza;
use Pizzaria\Bebida\Refrigerante\{
    CocaCola,
    Sprite
};
use Pizzaria\Pizza\Sabor\{
    Calabresa,
    Chocolate,
    Portuguesa,
    TresQueijos
};
use Pizzaria\Pizza\Tamanho\{
    Pequena,
    Grande
};


$pedido = new Pedido(random_int(1, 999));

$pedido->add(new Pizza(new Grande(new Portuguesa(), new Calabresa(), new TresQueijos())));
$pedido->add(new Pizza(new Pequena(new Chocolate())));
$pedido->add(new CocaCola(3));
$pedido->add(new Sprite(3));

print_r($pedido->ingredientes());