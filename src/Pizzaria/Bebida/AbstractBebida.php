<?php


namespace Pizzaria\Bebida;


use Pizzaria\ItemInterface;

abstract class AbstractBebida implements ItemInterface
{
    private int $quantidade;

    public function __construct(int $quantidade)
    {
        $this->quantidade = $quantidade;
    }

    public function getQuantidade(): int
    {
        return $this->quantidade;
    }

    abstract public function getUnidade(): string;

    public function getNome(): string
    {
        $name = explode('\\', static::class);

        return end($name);
    }

    public function isArtesanal(): bool
    {
        return false;
    }

}