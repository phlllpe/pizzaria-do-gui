<?php

namespace Pizzaria\Bebida\Refrigerante;

use Pizzaria\Bebida\AbstractBebida;

class CocaCola extends AbstractBebida
{
    public function getUnidade(): string
    {
        return 'lt';
    }
}