<?php

namespace Pizzaria\Bebida\Refrigerante;

use Pizzaria\Bebida\AbstractBebida;

class Sprite extends AbstractBebida
{
    public function getUnidade(): string
    {
        return 'lt';
    }


}