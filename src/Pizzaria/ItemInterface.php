<?php


namespace Pizzaria;


interface ItemInterface
{
    public function isArtesanal(): bool;
}