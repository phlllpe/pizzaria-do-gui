<?php


namespace Pizzaria;

use ArrayObject as PHPArrayObject;
use Pizzaria\Pizza\Pizza;
use Pizzaria\Pizza\Sabor\AbstractSabor;
use Pizzaria\Pizza\Sabor\Ingrediente\AbstractIngrediente;

class Pedido
{
    private int $id;
    private PHPArrayObject $itens;

    public function __construct(int $id)
    {
        $this->itens = new PHPArrayObject();
        $this->id = $id;
    }

    public function add(ItemInterface $item)
    {
        $this->itens->append($item);
    }

    /**
     * @var ItemInterface $item
     * @var Pizza $item
     * @var AbstractSabor $sabor
     * @var AbstractIngrediente $ingrediente
     */
    public function ingredientes(): array
    {
        $resume = [];
        foreach ($this->itens as $item) {
            if (!$item->isArtesanal()) {
                continue;
            }

            foreach ($item->getTamanho()->getSabores() as $sabor) {
                foreach ($sabor->getIngredientes() as $ingrediente) {
                    $qt = $resume[$ingrediente->getNome()]['quantidade'] ?? 0;
                    $resume[$ingrediente->getNome()]['quantidade'] = $qt + $ingrediente->getQuantidade();
                    $resume[$ingrediente->getNome()]['unidade'] = $ingrediente->getUnidade();
                }
            }
        }

        return $resume;
    }
}