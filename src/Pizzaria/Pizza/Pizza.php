<?php


namespace Pizzaria\Pizza;

use Pizzaria\ItemInterface;
use Pizzaria\Pizza\Tamanho\AbstractTamanho;

class Pizza implements ItemInterface
{
    private AbstractTamanho $tamanho;

    /**
     * Pizza constructor.
     * @param AbstractTamanho $tamanho
     */
    public function __construct(AbstractTamanho $tamanho)
    {
        $this->tamanho = $tamanho;
    }

    /**
     * @return AbstractTamanho
     */
    public function getTamanho(): AbstractTamanho
    {
        return $this->tamanho;
    }

    public function isArtesanal(): bool
    {
        return true;
    }

}