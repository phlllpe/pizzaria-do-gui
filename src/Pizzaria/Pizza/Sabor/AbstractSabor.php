<?php


namespace Pizzaria\Pizza\Sabor;

use ArrayObject as PHPArrayObject;
use Pizzaria\Pizza\Sabor\Ingrediente\AbstractIngrediente;

abstract class AbstractSabor
{
    private PHPArrayObject $ingredientes;

    public function __construct()
    {
        $this->ingredientes = new PHPArrayObject();
        $this->build();
    }

    abstract public function build(): void;

    public function addIngrediente(AbstractIngrediente $ingrediente): void
    {
        $this->ingredientes->append($ingrediente);
    }

    public function getIngredientes(): PHPArrayObject
    {
        return $this->ingredientes;
    }
}