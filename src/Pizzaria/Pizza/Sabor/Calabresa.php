<?php


namespace Pizzaria\Pizza\Sabor;


use Pizzaria\Pizza\Sabor\Ingrediente\Cebola;
use Pizzaria\Pizza\Sabor\Ingrediente\LinguicaCalabresa;
use Pizzaria\Pizza\Sabor\Ingrediente\MolhoTomate;
use Pizzaria\Pizza\Sabor\Ingrediente\Mussarela;

class Calabresa extends AbstractSabor
{
    public function build(): void
    {
        $this->addIngrediente(new MolhoTomate(200));
        $this->addIngrediente(new Mussarela(450));
        $this->addIngrediente(new Cebola(2));
        $this->addIngrediente(new LinguicaCalabresa(200));
    }

}