<?php


namespace Pizzaria\Pizza\Sabor;


use Pizzaria\Pizza\Sabor\Ingrediente\DiamanteNegro;
use Pizzaria\Pizza\Sabor\Ingrediente\Mussarela;

class Chocolate extends AbstractSabor
{
    public function build(): void
    {
        $this->addIngrediente(new Mussarela(450));
        $this->addIngrediente(new DiamanteNegro(200));
    }

}