<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


abstract class AbstractIngrediente
{
    private float $quantidade;

    public function __construct(float $quantidade)
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return float
     */
    public function getQuantidade(): float
    {
        return $this->quantidade;
    }

    abstract public function getUnidade(): string;

    public function getNome(): string
    {
        $name = explode('\\', static::class);

        return end($name);
    }

}