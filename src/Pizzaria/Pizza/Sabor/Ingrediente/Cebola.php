<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class Cebola extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'und';
    }

}