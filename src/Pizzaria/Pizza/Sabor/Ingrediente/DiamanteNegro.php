<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class DiamanteNegro extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'g';
    }

}