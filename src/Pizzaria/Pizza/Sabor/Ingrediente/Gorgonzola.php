<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class Gorgonzola extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'g';
    }

}