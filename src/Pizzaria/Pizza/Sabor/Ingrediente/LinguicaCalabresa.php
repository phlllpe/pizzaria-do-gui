<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class LinguicaCalabresa extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'g';
    }

}