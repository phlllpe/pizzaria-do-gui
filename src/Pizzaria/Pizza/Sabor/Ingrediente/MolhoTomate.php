<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class MolhoTomate extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'ml';
    }

}