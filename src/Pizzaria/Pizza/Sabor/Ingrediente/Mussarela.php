<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class Mussarela extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'g';
    }

}