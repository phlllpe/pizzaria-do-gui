<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class Pimentao extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'und';
    }

}