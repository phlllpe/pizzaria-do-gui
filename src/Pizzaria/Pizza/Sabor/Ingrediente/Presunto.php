<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class Presunto extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'g';
    }

}