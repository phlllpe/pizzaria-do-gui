<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class Provolone extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'g';
    }

}