<?php


namespace Pizzaria\Pizza\Sabor\Ingrediente;


class Tomate extends AbstractIngrediente
{
    public function getUnidade(): string
    {
        return 'und';
    }

}