<?php


namespace Pizzaria\Pizza\Sabor;


use Pizzaria\Pizza\Sabor\Ingrediente\Cebola;
use Pizzaria\Pizza\Sabor\Ingrediente\MolhoTomate;
use Pizzaria\Pizza\Sabor\Ingrediente\Pimentao;
use Pizzaria\Pizza\Sabor\Ingrediente\Presunto;
use Pizzaria\Pizza\Sabor\Ingrediente\Mussarela;
use Pizzaria\Pizza\Sabor\Ingrediente\Tomate;

class Portuguesa extends AbstractSabor
{
    public function build(): void
    {
        $this->addIngrediente(new MolhoTomate(200));
        $this->addIngrediente(new Mussarela(250));
        $this->addIngrediente(new Presunto(250));
        $this->addIngrediente(new Cebola(1));
        $this->addIngrediente(new Tomate(1));
        $this->addIngrediente(new Pimentao(1));
    }

}