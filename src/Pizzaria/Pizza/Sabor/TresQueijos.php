<?php


namespace Pizzaria\Pizza\Sabor;


use Pizzaria\Pizza\Sabor\Ingrediente\Gorgonzola;
use Pizzaria\Pizza\Sabor\Ingrediente\MolhoTomate;
use Pizzaria\Pizza\Sabor\Ingrediente\Mussarela;
use Pizzaria\Pizza\Sabor\Ingrediente\Provolone;

class TresQueijos extends AbstractSabor
{
    public function build(): void
    {
        $this->addIngrediente(new MolhoTomate(200));
        $this->addIngrediente(new Mussarela(250));
        $this->addIngrediente(new Provolone(250));
        $this->addIngrediente(new Gorgonzola(100));
    }

}