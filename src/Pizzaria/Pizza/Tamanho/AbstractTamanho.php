<?php


namespace Pizzaria\Pizza\Tamanho;

use ArrayObject as PHPArrayObject;
use InvalidArgumentException as PHPInvalidArgumentException;
use Pizzaria\Pizza\Sabor\AbstractSabor;

abstract class AbstractTamanho
{
    private PHPArrayObject $sabores;

    abstract public function quantidadeSabor(): int;

    public function __construct(AbstractSabor ...$sabores)
    {
        $this->sabores = new PHPArrayObject();
        $this->addSabores($sabores);
    }

    public function addSabores(array $sabores): void
    {
        foreach ($sabores as $sabor) {
            $this->addSabor($sabor);
        }
    }

    public function addSabor(AbstractSabor $sabor): void
    {
        if ($this->quantidadeSabor() == $this->sabores->count()) {
            throw new PHPInvalidArgumentException('Essa Pizza ja está completa');
        }

        $this->sabores->append($sabor);
    }

    public function getSabores(): PHPArrayObject
    {
        return $this->sabores;
    }

}