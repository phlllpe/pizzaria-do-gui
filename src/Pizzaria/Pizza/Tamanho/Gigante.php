<?php


namespace Pizzaria\Pizza\Tamanho;

class Gigante extends AbstractTamanho
{
    public function quantidadeSabor(): int
    {
        return 4;
    }

}