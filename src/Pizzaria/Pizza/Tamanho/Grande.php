<?php


namespace Pizzaria\Pizza\Tamanho;

use Pizzaria\Pizza\Sabor\AbstractSabor;

class Grande extends AbstractTamanho
{

    public function quantidadeSabor(): int
    {
        return 3;
    }

}