<?php


namespace Pizzaria\Pizza\Tamanho;


class Pequena extends AbstractTamanho
{
    public function quantidadeSabor(): int
    {
        return 1;
    }

}